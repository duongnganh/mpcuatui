package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class PicturesFolder extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures_folder);

        ListView PicturesListView = (ListView) findViewById(R.id.PicturesListView);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        final ArrayList<File> picturesFiles = (ArrayList) b.getParcelableArrayList("picturesFiles");
        File currentFile;

        int size = picturesFiles.size();
        String[] imgTitles = new String[size];
        for (int j = 0; j < size; j++) {
            currentFile = picturesFiles.get(j);

            imgTitles[j] = currentFile.getName().toString()
                    .replace(".png", "").replace(".jpg", "");
        }

        ArrayAdapter<String> picturesAdapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.music_layout, R.id.textView, imgTitles);
        PicturesListView.setAdapter(picturesAdapter);
        PicturesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), PicturesShow.class)
                        .putExtra("position", position).putExtra("imglist", picturesFiles));
            }
        });
    }
}
