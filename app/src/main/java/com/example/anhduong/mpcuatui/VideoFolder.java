package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class VideoFolder extends AppCompatActivity {
    String[] videoTitles;
    ListView VideoListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_folder);

        VideoListView = (ListView) findViewById(R.id.VideoListView);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        final ArrayList<File> videoFiles = (ArrayList) b.getParcelableArrayList("videoFiles");
        File currentFile;

        int size = videoFiles.size();
        videoTitles = new String[size];
        for (int j = 0; j < size; j++){
            currentFile = videoFiles.get(j);

            videoTitles[j] = currentFile.getName().toString()
                    .replace(".mp4", "").replace(".wmv", "");
        }

        ArrayAdapter<String> videoAdapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.music_layout,R.id.textView, videoTitles);
        VideoListView.setAdapter(videoAdapter);

        VideoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), SongsPlayer.class)
                        .putExtra("position", position).putExtra("songlist", videoFiles));
            }
        });
    }
}
