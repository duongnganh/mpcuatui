package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout MusicLinearLayout = (LinearLayout) findViewById(R.id.Music2);
        LinearLayout VideoLinearLayout = (LinearLayout) findViewById(R.id.Video2);
        LinearLayout PicturesLinearLayout = (LinearLayout) findViewById(R.id.Pictures2);

        TextView MusicItemTextView = (TextView) findViewById(R.id.MusicItemTextView);
        TextView VideoItemTextView = (TextView) findViewById(R.id.VideoItemTextView);
        TextView PicturesItemTextView = (TextView) findViewById(R.id.PicturesItemTextView);

        //Music - Video - Pictures
        ArrayList<File>[] allFiles = findItems(Environment.getExternalStorageDirectory());
        final ArrayList<File> musicFiles = allFiles[0];
        final ArrayList<File> videoFiles = allFiles[1];
        final ArrayList<File> picturesFiles = allFiles[2];

        MusicItemTextView.setText(musicFiles.size()+" item(s)");
        VideoItemTextView.setText(videoFiles.size() + " item(s)");
        PicturesItemTextView.setText(picturesFiles.size() + " item(s)");

        MusicLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent musicIntent = new Intent(getApplicationContext(),
                        MusicFolder.class).putExtra("musicFiles", musicFiles);
                startActivity(musicIntent);
            }
        });

        VideoLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videoIntent = new Intent(getApplicationContext(),
                        VideoFolder.class).putExtra("videoFiles", videoFiles);
                startActivity(videoIntent);
            }
        });

        PicturesLinearLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent picturesIntent = new Intent(getApplicationContext(),
                        PicturesFolder.class).putExtra("picturesFiles",picturesFiles);
                startActivity(picturesIntent);
            }
        });

    }

    public ArrayList<File>[] findItems(File root){
        ArrayList<File>[] al = new ArrayList[3];
        ArrayList<File>[] nextal = new ArrayList[3];
        String filename;
        for (int i = 0; i < 3; i ++){
            al[i] = new ArrayList<>();
            nextal[i] = new ArrayList<>();
        }
        File[] files = root.listFiles();
        if (files.length > 0)
        for (File singleFile : files){
            if (singleFile.isDirectory() && !singleFile.isHidden()){
                nextal = findItems(singleFile);
                for (int i = 0; i < 3; i++){
                    al[i].addAll(nextal[i]);
                }
            }
            else{
                filename = singleFile.getName();
                if (filename.endsWith(".mp3")){
                    al[0].add(singleFile);
                }
                if (filename.endsWith(".mp4") || filename.endsWith(".wmv")){
                    al[1].add(singleFile);
                }
                if (filename.endsWith(".jpg") || filename.endsWith(".png")){
                    al[2].add(singleFile);
                }
            }
        }
        return al;
    }
}
