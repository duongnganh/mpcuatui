package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

public class PicturesShow extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures_show);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        int position = b.getInt("position");
        ArrayList<File> picturesFiles = (ArrayList) b.getParcelableArrayList("imglist");
        File f = picturesFiles.get(position);

        ImageView singleImageView = (ImageView) findViewById(R.id.singleImageView);
        singleImageView.setImageURI(Uri.parse(f.toString()));
        singleImageView.setScaleType(ImageView.ScaleType.FIT_XY);
    }
}
