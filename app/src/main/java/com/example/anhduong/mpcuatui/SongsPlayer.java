package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class SongsPlayer extends AppCompatActivity implements View.OnClickListener{
    static MediaPlayer mp;
    SeekBar SongsSeekBar;
    Button SongsNxtBt, SongsPrevBt, SongsPlayBt;
    ArrayList<File> songlist;
    int position;
    Uri u;
    TextView SongsTextView, ArtistTextView;
    boolean isMpFinished;
    refreshSeekbar refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs_player);

        SongsTextView = (TextView) findViewById(R.id.SongsTextView);
        ArtistTextView = (TextView) findViewById(R.id.ArtistTextView);

        SongsPlayBt = (Button) findViewById(R.id.SongsPlayBt);
        SongsNxtBt = (Button) findViewById(R.id.SongsNxtBt);
        SongsPrevBt = (Button) findViewById(R.id.SongsPrevBt);

        SongsPlayBt.setOnClickListener(this);
        SongsNxtBt.setOnClickListener(this);
        SongsPrevBt.setOnClickListener(this);

        SongsSeekBar = (SeekBar) findViewById(R.id.SongsSeekBar);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        songlist = (ArrayList) b.getParcelableArrayList("songlist");

        isMpFinished = true;
        position = b.getInt("position", 0);
        update();
        if (refresh != null){
            refresh.cancel(true);
        }
        refresh = new refreshSeekbar();
        refresh.execute();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id){
            case R.id.SongsPlayBt:
                if (mp.isPlaying()){
                    SongsPlayBt.setText("\u25B6");
                    mp.pause();
                }
                else{
                    SongsPlayBt.setText("||");
                    mp.start();
                }
                break;
            case R.id.SongsNxtBt:
                position = (position+1)%songlist.size();
                update();
                break;
            case R.id.SongsPrevBt:
                position = (position-1+songlist.size())%songlist.size();
                update();
                break;
        }
    }

    public void update(){
        if (mp!=null) {
            mp.stop();
            mp.release();
        }

        u = Uri.parse(songlist.get(position).toString());
        mp = MediaPlayer.create(getApplicationContext(), u);

        SongsSeekBar.setMax(mp.getDuration());

        File currentFile = songlist.get(position);
        String songname = currentFile.getName().replace(".mp3", "").replace("wav", "");

        Uri uri = Uri.fromFile(currentFile);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(getApplicationContext(), uri);
        String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);

        SongsTextView.setText(songname);
        ArtistTextView.setText(artist);

        mp.start();
        SongsPlayBt.setText("||");
        isMpFinished = false;

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                position = (position+1)%songlist.size();
                update();
            }
        });

        SongsSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mp.seekTo(SongsSeekBar.getProgress());
            }
        });
    }

    public class refreshSeekbar extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            while (!isMpFinished){
                SystemClock.sleep(200);
                publishProgress();
                if (isCancelled()) break;
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            int currPos = mp.getCurrentPosition();
            SongsSeekBar.setProgress(currPos);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            SongsSeekBar.setProgress(mp.getDuration());
        }
    }
}

