package com.example.anhduong.mpcuatui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class MusicFolder extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_folder);

        final String[] songTitles;
        ListView SongsListView = (ListView) findViewById(R.id.SongsListView);
        int size;

        Intent i = getIntent();
        Bundle b = i.getExtras();
        final ArrayList<File> musicFiles = (ArrayList) b.getParcelableArrayList("musicFiles");
        File currentFile;

        size = musicFiles.size();
        songTitles = new String[size];
        for (int j = 0; j < size; j++){
            currentFile = musicFiles.get(j);

            songTitles[j] = currentFile.getName().toString()
                    .replace(".mp3", "").replace(".wav", "");
        }

        ArrayAdapter<String> musicAdapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.music_layout,R.id.textView, songTitles);
        SongsListView.setAdapter(musicAdapter);
        SongsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getApplicationContext(), SongsPlayer.class)
                        .putExtra("position", position).putExtra("songlist", musicFiles));
            }
        });
    }
}
